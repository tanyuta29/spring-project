import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;
@Component
public class Calculator {
@Autowired
    static Scanner scanner;
    public static int getInt(){
        System.out.println("Enter number");
        int num;
        if (scanner.hasNextInt()){
            num = scanner.nextInt();
        }else {
            System.out.println("You made error, try agan");
            scanner.next();
            num = getInt();
        }
        return num;
    }
}
