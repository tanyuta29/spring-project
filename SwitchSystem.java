import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SwitchSystem {
    @Autowired
    Operation operation1;
    public static int calc(int num1, int num2, char operation){

        int result;
        switch (operation){
            case '+':
                result=num1+num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '*':
                result = num1*num2;
                break;
            case '/':
                result = num1/num2;
                break;
            default:
                System.out.println("Try agan, operation not successful");
       result = calc(num1,num2,Operation.getOperation());
        }
        return result;
    }
}
