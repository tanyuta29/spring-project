import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class Calculate {
    @Autowired
    Operation operation;
    Calculate calculate;
    Scanner scanner;
    SwitchSystem switchSystem;
    public static void main(String[] args) {
        int num1 = Calculator.getInt();
        int num2 = Calculator.getInt();
        char operation = Operation.getOperation();
        int result = SwitchSystem.calc(num1,num2,operation);
        System.out.println("Answer "+result);
    }
}
